php artisan migrate
php artisan db:seed --class=AnimalKindSeeder

# supervisorctl reread
# supervisorctl update
# supervisorctl -c /etc/supervisord.conf start default-queue-worker:*
# supervisorctl -c /etc/supervisord.conf start websockets-queue-worker:*
# supervisorctl -c /etc/supervisord.conf start websockets-worker:*
supervisord -c /etc/supervisord.conf

php artisan serve --host=0.0.0.0 --port=8000