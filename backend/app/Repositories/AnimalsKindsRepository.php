<?php

namespace App\Repositories;

use App\Models\AnimalsKind;

class AnimalsKindsRepository
{
    public function getAllAnimalsKinds() {
        return AnimalsKind::all();
    }

    public function getAnimalKind(string $kind) {
        return AnimalsKind::where("kind", $kind)->first();
    }

    public function createAnimalKind(array $fields) {
        return AnimalsKind::create($fields);
    }
}