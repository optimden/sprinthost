<?php

namespace App\Repositories;

use App\Models\Animal;
use Illuminate\Database\QueryException;
use App\Exceptions\ModelNotFoundException;

class AnimalsRepository
{
    public function getAnimalByName(string $name) {
        return Animal::where("name", $name)->first();
    }

    public function createAnimal(array $fields) {
        // Проверяем, задал ли пользователь начальные размер
        // и возраст животного
        $isAgePresent = !is_null($fields["age"]);
        $isSizePresent = !is_null($fields["size"]);
        if (!$isAgePresent || !$isSizePresent) {
            // Получаем модель животного данного типа
            $kind = $fields["kind"];
            $animalKind = AnimalsKindsRepository::getAnimalKind($kind);
            if (is_null($animalKind)) {
                throw new ModelNotFoundException("AnimalKind with kind " . $kind . " not found.");
            }
        }

        // Устанавливаем дефолтные значения
        $fields["age"] = $isAgePresent ? $fields["age"] : $animalKind->default_age;
        $fields["size"] = $isAgePresent ? $fields["size"] : $animalKind->default_size;

        return Animal::create($fields);
    }

    public function removeAnimal(string $name) {
        Animal::destroy($name);
    }

    public function updateAnimalAgeSize(string $name, int $age, int $size) {
        $animal = static::getAnimalByName($name);
        if (is_null($animal)) {
            throw new ModelNotFoundException("Animal with name " . $name . " was not found.");
        }
        $animal->age = $age;
        $animal->size = $size;
        $animal->save();
        return $animal;
    }
}