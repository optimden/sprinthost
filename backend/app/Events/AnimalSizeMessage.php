<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class AnimalSizeMessage implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $size;
    public $name;
    public $age;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(int $size, int $age, string $name)
    {
        $this->size = $size;
        $this->name = $name;
        $this->age = $age;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel("animalSize");
    }

    public function broadcastWith() {
        return [
            "size" => $this->size,
            "name" => $this->name,
            "age" => $this->age
        ];
    }

    public function broadcastQueue() {
        return "websockets";
    }

    public function broadcastAs() {
        return "AnimalSizeMessage";
    }
}
