<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Animal extends Model 
{
    use HasFactory;

    protected $primaryKey = 'name';
    public $incrementing = false;

    protected $fillable = [
        'name',
        'kind',
        'age',
        'size'
    ];

    public function animalKind() {
        return $this->belongsTo(AnimalKind::class);
    }
}