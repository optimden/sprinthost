<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AnimalsKind extends Model 
{
    use HasFactory;

    protected $primaryKey = 'kind';
    public $incrementing = false;

    protected $fillable = [
        'kind',
        'max_size',
        'max_age',
        'growth_factor',
        'image_path',
        'default_age',
        'default_size'
    ];

    public function animals() {
        return $this->hasMany(Animal::class);
    }
}