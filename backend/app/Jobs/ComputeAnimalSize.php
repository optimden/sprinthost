<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;

use App\Events\AnimalSizeMessage;
use App\Models\Animal;
use App\Repositories\AnimalsRepository;
use App\Repositories\AnimalsKindsRepository;
use App\Exceptions\ModelNotFoundException;

class ComputeAnimalSize implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable;

    public $animal;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($animal)
    {
        $this->animal = $animal;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Получаем исходные размер и возраст животного
        $initialSize = (integer)$this->animal->size;
        $age = (integer)$this->animal->age;

        // Получаем имя животного
        $name = $this->animal->name;

        // Получаем модель данного типа животного
        $kind = $this->animal->kind;
        $animalKind = AnimalsKindsRepository::getAnimalKind($kind);

        // Получаем необходимые параметры данного типа
        $growthFactor = $animalKind->growth_factor;
        $maxSize = (integer)$animalKind->max_size + 50;
        $maxAge = (integer)$animalKind->max_age;

        $size = $initialSize + 50;
        $sizeStep = $growthFactor * 10;

        $iterationCount = floor(($maxSize - $initialSize) / $sizeStep);

        $ageStep = ($maxAge - $age) / $iterationCount;

        // Каждые $growth_factor или фиксированное кол-во секунд инициируем событие отправки
        // пересчитанного размера по вебсокету
        $uiSize = $size;
        while ($size < $maxSize) {
            // Пишем в БД обновленные размер и возраст животного
            $dbSize = $size - 50;
            try {
                $updateAnimal = AnimalsRepository::updateAnimalAgeSize($name, $age, $dbSize);
            } catch (ModelNotFoundException $ex) {
                return;
            }
            
            event(new AnimalSizeMessage($uiSize, $age, $name));
            sleep(7);
            $uiSize = floor($uiSize + $sizeStep);
            $size = floor($size + ($sizeStep / 5));
            $age = floor($age + $ageStep); 
        }
    }
}
