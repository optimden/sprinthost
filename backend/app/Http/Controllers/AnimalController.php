<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\AnimalsRepository;
use App\Repositories\AnimalsKindsRepository;
use App\Exceptions\ModelNotFoundException;
use App\Jobs\ComputeAnimalSize;

class AnimalController extends Controller 
{

    public function createAnimal(Request $request) {
        $fields = $request->validate([
            "name" => "required",
            "kind" => "required",
            "age" => "nullable",
            "size" => "nullable"
        ]);

        try {
            $animal = AnimalsRepository::createAnimal($fields);
            ComputeAnimalSize::dispatch($animal);
            return response()->json([
                "error" => null,
                "data" => "ok"
            ], 201);
        } catch(\Exception $ex) {
            return response()->json([
                "error" => $ex->getMessage(),
                "data" => "error"
            ], 500);
        }
    }

    public function removeAnimal(Request $request) {
        $name = $request->name;
        try {
            AnimalsRepository::removeAnimal($name);
            return response()->json([
                "err" => null,
            ], 200);
        } catch (\Exception $ex) {
            return response()->json([
                "err" => $ex->getMessage()
            ], 500);
        }
        
    }

    public function getAnimalKinds(Request $request) {
        $animalKinds = AnimalsKindsRepository::getAllAnimalsKinds();
        return response()->json([
            "error" => null,
            "data" => $animalKinds
        ], 200);
    }
} 