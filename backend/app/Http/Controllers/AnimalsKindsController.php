<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class AnimalsKindsController extends Controller
{

    public function createAnimalKind(Request $request) {
        $fields = $request->validate([
            "kind" => "required",
            "max_size" => "required",
            "max_age" => "required",
            "growth_factor" => "required"
        ]);

        $image = $request->file("image");

        if (is_null($image)) {
            return response()->json([

            ], 400);
        }

        Storage::disk("local")->put($fields["kind"] . "png", $image);

        $animalKind = AnimalsKindsRepository::createAnimalKind($fields);
        return response()->json([
            "error" => null,
            "data" => "ok"
        ], 201);
    }

    public function getAllAnimalsKinds(Request $request) {

    }
}