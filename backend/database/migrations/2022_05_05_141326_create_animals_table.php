<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnimalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('animals', function (Blueprint $table) {
            $table->string('name');
            $table->primary('name');
            $table->string('kind');
            $table->foreign('kind')->references('kind')->on('animals_kinds')->onDelete('cascade');
            $table->integer('age');
            $table->integer('size');
            $table->timestamps();
        });

        // TODO: add triggers or functions for check constraint
        // for check if size and age greater then max_size and
        // max_age for current animal type
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('animals');
    }
}
