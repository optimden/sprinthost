<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Repositories\AnimalsKindsRepository;

class AnimalKindSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $kinds = [];
        $wolf = [
            "kind" => "wolf",
            "max_size" => 45,
            "max_age" => 80,
            "growth_factor" => 1.7,
            "image_path" => "",
            "default_size" => 5,
            "default_age" => 0
        ];
        $pig = [
            "kind" => "pig",
            "max_size" => 56,
            "max_age" => 43,
            "growth_factor" => 1.5,
            "image_path" => "",
            "default_size" => 10,
            "default_age" => 0
        ];
        $dog = [
            "kind" => "dog",
            "max_size" => 44,
            "max_age" => 61,
            "growth_factor" => 1.3,
            "image_path" => "",
            "default_size" => 7,
            "default_age" => 0
        ];
        $bird = [
            "kind" => "bird",
            "max_size" => 27,
            "max_age" => 101,
            "growth_factor" => 0.7,
            "image_path" => "",
            "default_size" => 3,
            "default_age" => 0
        ];
        array_push($kinds, $wolf);
        array_push($kinds, $pig);
        array_push($kinds, $dog);
        array_push($kinds, $bird);
        foreach($kinds as $kind) {
            AnimalsKindsRepository::createAnimalKind($kind);
        }
    }
}
