import { createApp } from 'vue'
import App from './App.vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import mitt from 'mitt'
import Echo from 'laravel-echo'

const app = createApp(App)
app.use(VueAxios, axios)

const emitter = mitt()
app.config.globalProperties.emitter = emitter

window.Pusher = require('pusher-js')

window.Echo = new Echo({
    broadcaster: 'pusher',
    namespace: '',
    key: '12345',
    wsHost: '0.0.0.0',
    wsPort: '6001',
    enabledTransports: ['ws', 'wss']
})

app.mount('#app')
